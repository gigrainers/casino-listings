<?php

if (!defined('ABSPATH')) {
	die;
}

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://yoursite.lv
 * @since             1.0.0
 * @package           Casino_Listings
 *
 * @wordpress-plugin
 * Plugin Name:       Casino Listings
 * Plugin URI:        https://yoursite.lv
 * Description:       Create different casino toplists, boxes and widgets
 * Version: 		  1.1.18
 * Author:            Kristaps Ritins
 * Author URI:        https://yoursite.lv
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       casino-listings
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
defined('CASINO_LISTINGS_VERSION') or define('CASINO_LISTINGS_VERSION', '1.0.18');

defined('CASINO_LISTINGS_NAME') or define('CASINO_LISTINGS_NAME', 'casino-listings');

/**
 * Plugin directory path
 */
defined('CASINO_LISTINGS_BASE_DIR') or define('CASINO_LISTINGS_BASE_DIR', plugin_dir_path(__FILE__));

/**
 * Plugin directory URL
 */
defined('CASINO_LISTINGS_PLUGIN_URL') or define('CASINO_LISTINGS_PLUGIN_URL', plugin_dir_url(__FILE__));


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-casino-listings-activator.php
 */
function activate_casino_listings()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-casino-listings-activator.php';
	Casino_Listings_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-casino-listings-deactivator.php
 */
function deactivate_casino_listings()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-casino-listings-deactivator.php';
	Casino_Listings_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_casino_listings');
register_deactivation_hook(__FILE__, 'deactivate_casino_listings');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-casino-listings.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_casino_listings()
{
	$plugin = new Casino_Listings();
	$plugin->run();
}
require plugin_dir_path(__FILE__) . 'includes/plugin-auto-updater.php';
function YSG_plugin_activate_autoupdates()
{
	$current_version = get_file_data(__FILE__, array("Version" => "Version"), false);
	$current_version = $current_version["Version"];
	$plugin_path = plugin_basename(__FILE__);
	$api_url = "https://buildbot.gigmedia.tech/plugins?action=";

	new YSG_Plugin_AutoUpdater($current_version, $plugin_path, $api_url);
}
add_action("init", "YSG_plugin_activate_autoupdates");

run_casino_listings();