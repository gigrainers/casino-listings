<?php

include CASINO_LISTINGS_BASE_DIR . 'templates/casino-meta-data.php';

?>

<div class="casino-box">
    <div class="casino-box_content-wrapper">
        <div class="casino-box_logo">
            <a href="<?php echo $affiliate_url; ?>" target="_blank"><img src="<?php echo $casino_image; ?>" alt=""></a>
        </div>
        <div class="casino-box_main-bonus">
            <?php
            if ($box_bonus_field) {
                foreach ($box_bonus_field as $field) {
                    echo '<span class="box__bonuses-title">' . $field . '</span>';
                }
            }
            ?>
        </div>
        <div class="casino-box_perks">
            <?php
            if ($list_bonus_field) {
                foreach ($list_bonus_field as $field) {
                    echo '<span class="box__bonus-title"><i class="fas fa-check"></i>' . $field . '</span>';
                }
            }
            ?>
        </div>
        <div class="casino-box_review"><a href="<?php echo get_post_permalink(); ?>" class="list__review"><?php echo __('Review', 'casino-listings'); ?></a></div>
        <div class="casino-box_cta">
            <a class="box__button hvr-icon-forward" href="<?php echo $affiliate_url; ?>" <?php if (get_option('cl_btn_color_4')) {
                                                                                                echo "style= background:" . get_option('cl_btn_color_4');
                                                                                            } ?> target="_blank"><?php echo __('Register', 'casino-listings'); ?><i class="fas fa-angle-double-right hvr-icon"></i></a>
        </div>
    </div>
</div>