<?php

include CASINO_LISTINGS_BASE_DIR . 'templates/casino-meta-data.php';

?>
<div class="cas-list-wrap">
    <div class="casino-list" <?php if ($atts['itemlist'] == 'on') : echo 'itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"';
                                endif; ?>>
        <span class="position" style="display: none;" <?php if ($atts['itemlist'] == 'on') : echo 'itemprop="position"';
                                                        endif; ?>><?php echo $number; ?></span>
        <span itemprop="name" style="display: none;"><?php the_title(); ?></span>
        <?php if (get_post_meta(get_the_ID(), 'cl_banner', 1)) : ?>
            <div class="rib" style="background-color:<?php echo $banner_color; ?>"><?php echo $banner_text; ?></div>
        <?php endif; ?>
        <div class="list__rank">
            <div class="list__rank-position"></div>
        </div>
        <div class="list__logo">
            <div class="list__logo-wrap">
                <?php if ($decomissioned) : ?>
                    <div class="list__logo-image"><?php the_post_thumbnail() ?></div>
                <?php else : ?>
                    <div class="list__logo-image"><a href="<?php echo $affiliate_url; ?>" target="_blank"><?php the_post_thumbnail() ?></a></div>
                <?php endif; ?>
                <span class="stars">
                    <?php
                    if ($casino_rating) {
                        for ($i = 1; $i <= 5; $i++) {
                            if (round($casino_rating - .25) >= $i) {
                                echo "<i class='fa fa-star'></i>";
                            } elseif (round($casino_rating + .25) >= $i) {
                                echo "<i class='fa fa-star-half-o'></i>";
                            } else {
                                echo "<i class='far fa-star'></i>";
                            }
                        }
                    }
                    ?>
                </span>
            </div>
        </div>
        <div class="list__bonus-container">
            <div class="list__bonus">
                <?php
                $counter = 0;
                if ($box_bonus_field) {
                    foreach ($box_bonus_field as $field) {
                        if ($counter == 1) {
                            echo "<a href='{$affiliate_url}' class='list__bonus-title' target='_blank'><span>" . $field . '</span></a>';
                        } else {
                            echo '<span class="list__bonus-title">' . $field . '</span>';
                        }
                        $counter++;
                    }
                }
                ?>
            </div>
            <div class="list__highlight">
                <span class="list__highlight-title">
                    <?php echo $casino_highlight; ?>
                </span>
                <div class="methods-wrap">
                    <span class="list__highlight-subtitle"><?php echo __('Payment options:', 'casino-listings'); ?></span>
                    <div class="list__highlight-deposit">
                        <?php
                        $num = 0;
                        $max = 5;
                        if ($deposit_methods) {
                            foreach ($deposit_methods as $method) {
                                if ($method && $num < $max) {
                                    echo "<div class='payment-card'><img class='$method' title='$method' src=" . CASINO_LISTINGS_PLUGIN_URL . "public/img/$method.jpg alt='$method'></div>";
                                } else {
                                    break;
                                }
                                $num++;
                            }
                        }
                        ?>
                    </div>
                    <div class="withdrawal-time">
                        <?php if (get_post_meta(get_the_ID(), 'cl_review_withdraw', true)) : ?>
                            <?php echo __("Time for withdrawal:", 'casino-listings'); ?><span><?php echo " " . get_post_meta(get_the_ID(), 'cl_review_withdraw', true); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="list__top">
            <?php
            if ($list_bonus_field) {
                foreach ($list_bonus_field as $field) {
                    echo '<span class="list__top3"><i class="fas fa-check"></i>' . $field . '</span>';
                }
            }
            ?>
        </div>
        <div class="list__cta" <?php if ($decomissioned) echo 'style="grid-template-rows:50% 50%;"' ?>>
            <div class="list__more-info">
                <div class="list__quick-facts hvr-icon-fade"><?php echo __('Facts', 'casino-listings'); ?><i class="fas fa-chevron-circle-down"></i></div>
            </div>
            <?php if ($decomissioned) : ?>

            <?php else : ?>
                <a class="list__button hvr-icon-forward" <?php if (get_option('cl_btn_color_1')) {
                                                                echo "style= background-color:" . get_option('cl_btn_color_1');
                                                            } ?> href="<?php echo $affiliate_url; ?>" target="_blank"><?php echo __('Register', 'casino-listings'); ?><i class="fas fa-angle-double-right hvr-icon"></i></a>
            <?php endif; ?>
            <a href="<?php echo get_post_permalink(); ?>" class="list__review" <?php if ($atts['itemlist'] == 'on') : echo 'itemprop="url"';
                                                                                endif; ?>><?php echo __('Review', 'casino-listings'); ?></a>
        </div>
    </div>
    <div class="dropdown-wrap hidden">
        <?php include CASINO_LISTINGS_BASE_DIR . 'templates/shortcodes/casino-list-info.php'; ?>
    </div>
</div>
<?php if (get_post_meta(get_the_ID(), 'cl_terms', 1)) : ?>
    <div class="cl-terms">
        <div class="cl-terms__text"><i class="fas fa-info-circle"></i><?php echo $terms_conditions; ?></div>
    </div>
<?php endif; ?>