<?php $max_rating = 5; ?>
<div class="list__dropdown">
    <div class="list__dropdown-facts">
        <div class="dropdown-heading"><i class="fas fa-comment-dots"></i><?php echo __('Information', 'casino-listings'); ?></div>
        <div class="dropdown-facts-info">

            <?php if ($casino_launched) : ?>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Casino Established', 'casino-listings'); ?></div>
                    <div class="dd-info-right"><?php echo $casino_launched; ?></div>
                </div>
            <?php endif; ?>

            <?php if ($casino_livechat) : ?>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Live Chat', 'casino-listings'); ?></div>
                    <div class="dd-info-right"><i class="far fa-check-circle"></i></div>
                </div>
            <?php else : ?>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Live Chat', 'casino-listings'); ?></div>
                    <div class="dd-info-right"><i class="far fa-times-circle"></i></div>
                </div>
            <?php endif; ?>

            <?php if ($casino_mobile) : ?>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Mobile Version', 'casino-listings'); ?></div>
                    <div class="dd-info-right"><i class="far fa-check-circle"></i></div>
                </div>
            <?php else : ?>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Mobile Version', 'casino-listings'); ?></div>
                    <div class="dd-info-right"><i class="far fa-times-circle"></i></div>
                </div>
            <?php endif; ?>

            <?php if ($casino_min_deposit) : ?>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Min. Deposit', 'casino-listings'); ?></div>
                    <div class="dd-info-right"><?php echo $casino_min_deposit; ?></div>
                </div>
            <?php else : ?>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Min. Deposit', 'casino-listings'); ?></div>
                    <div class="dd-info-right">-</div>
                </div>
            <?php endif; ?>

            <?php if ($casino_rating) : ?>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Rating', 'casino-listings'); ?></div>
                    <div class="dd-info-right"><?php echo $casino_rating . "/{$max_rating}"; ?></div>
                </div>
            <?php else : ?>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Rating', 'casino-listings'); ?></div>
                    <div class="dd-info-right">-</div>
                </div>
            <?php endif; ?>

        </div>
    </div>
    <div class="list__dropdown-intro">
        <div class="dropdown-heading"><i class="fas fa-database"></i><?php echo __('Introduction', 'casino-listings'); ?></div>
        <div class="dropdown-intro-text"><?php echo $casino_introduction; ?></div>
    </div>
    <div class="list__dropdown-bonus">
        <div class="dropdown-heading"><i class="fas fa-box-open"></i><?php echo __('Bonuses', 'casino-listings'); ?></div>
        <div class="dropdown-bonus-info">
            <div class="info-field-wrap">
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Bonus', 'casino-listings'); ?></div>
                    <div class="dd-info-right"><?php echo $casino_bonus; ?></div>
                </div>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Bonus Percentage', 'casino-listings'); ?></div>
                    <div class="dd-info-right"><?php echo $bonus_percentage; ?></div>
                </div>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Rollover', 'casino-listings'); ?></div>
                    <div class="dd-info-right"><?php echo $wager_requirements; ?></div>
                </div>
                <div class="dd-info-field">
                    <div class="dd-info-left"><?php echo __('Free Spins', 'casino-listings'); ?></div>
                    <div class="dd-info-right"><?php echo $free_spins; ?></div>
                </div>
            </div>
        </div>
        <div class="dd-buttons-field">
            <a class="dd-buttons-register hvr-icon-forward" <?php if (get_option('cl_btn_color_2')) {
                                                                echo "style= background:" . get_option('cl_btn_color_2');
                                                            } ?> href="<?php echo $affiliate_url; ?>" target="_blank"><?php echo __('Register', 'casino-listings'); ?><i class="fas fa-angle-double-right hvr-icon"></i></a>
            <a href="<?php echo get_post_permalink(); ?>" <?php if (get_option('cl_btn_color_3')) {
                                                                echo "style= background:" . get_option('cl_btn_color_3');
                                                            } ?> class="dd-buttons-review"><?php echo __('Review', 'casino-listings'); ?></a>
        </div>
    </div>
</div>