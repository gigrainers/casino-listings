<?php

if (!defined('ABSPATH')) {
	die;
}

include CASINO_LISTINGS_BASE_DIR . 'templates/casino-meta-data.php';

$maximum_rating = 10;

$ratings_array = [
	__('Bonus Offers', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_review_rating_bonus', true),
	__('Mobile Experience', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_review_rating_mobile', true),
	__('Payment Options', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_review_rating_payments', true),
	__('Customer Support', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_review_rating_support', true)
];

$general_array = [
	__('Year of foundation', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_information_launched', true),
	__('License', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_review_licence', true),
	__('E-mail', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_review_email', true),
	__('Minimum deposit', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_information_min_deposit', true),
	__('Time to withdraw', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_review_withdraw', true)
];

$bonus_array = [
	__('Bonus', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_information_casino_bonus', true),
	__('Bonus percentage', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_information_bonus_percentage', true),
	__('Wager', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_information_wager_requirements', true),
	__('Free spins', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_information_free_spins', true),
	__('Bonus code', 'casino-listings') => get_post_meta(get_the_ID(), 'cl_review_bonus_code', true)
];

?>


<section itemscope class="single-casino-wrapper" id="single-casino-id-<?php the_ID(); ?>">

	<?php if ($decomissioned) : ?>

	<?php else : ?>
		<div class="fixed-nav-top sm-hide">
			<div class="nav-top-container">
				<div class="nav-top-middle">
					<a href="<?php echo $affiliate_url; ?>"><?php echo __('Get Bonus', 'casino-listings'); ?><i class="fas fa-angle-double-right hvr-icon"></i></a>
					<span>
						<?php
						if ($box_bonus_field) {
							foreach ($box_bonus_field as $field) {
								echo "<span class='fixed-nav-bonuses'>" . $field . "</span>";
							}
						}
						?>
					</span>
				</div>
				<div class="nav-top-right"><img src="<?php echo $casino_image; ?>" alt="<?php echo get_post_meta($image_id, '_wp_attachment_image_alt', TRUE); ?>"></div>
			</div>
		</div>
	<?php endif; ?>


	<div class="prev-casino sm-hide">
		<?php

		$prev_post = get_adjacent_post(false, '', true);
		if (!empty($prev_post)) {
			echo '<a href="' . get_permalink($prev_post->ID) . '" title="' . $prev_post->post_title . '"><i class="fas fa-angle-double-left"></i>' . get_the_post_thumbnail($prev_post) . '</a>';
		}

		?>
	</div>

	<div class="next-casino sm-hide">
		<?php
		$next_post = get_adjacent_post(false, '', false);
		if (!empty($next_post)) {
			echo '<a href="' . get_permalink($next_post->ID) . '" title="' . $next_post->post_title . '">' . get_the_post_thumbnail($next_post) .  '<i class="fas fa-angle-double-right"></i></a>';
		}
		?>

	</div>


	<div class="upper-casino-wrap">
		<div class="casino-name">
			<div class="casino-name-top">
				<?php echo "<h1>" . get_the_title() . "</h1>"; ?>
			</div>
			<div class='casino-flags-bottom-mobile'>

				<?php
				if ($players_accepted) {
					foreach ($players_accepted as $flag) {
						echo "<img src=" . CASINO_LISTINGS_PLUGIN_URL . "public/img/flags/$flag.svg' alt='{$flag}-flag'>";
					}
				}

				?>
			</div>
		</div>
		<div class="main-top-part">

			<div class="upper-left-box">
				<div class="left-box__img-area">
					<a href="<?php echo $affiliate_url; ?>"><?php the_post_thumbnail() ?></a>
				</div>
				<div class="left-box__bonus-area">
					<div class="list__bonus">
						<?php
						$counter = 0;
						if ($box_bonus_field) {
							foreach ($box_bonus_field as $field) {
								if ($counter == 1) {
									echo "<a class='list__bonus-title' href='{$affiliate_url}'><span>" . $field . "</span></a>";
								} else {
									echo "<span class='list__bonus-title'>" . $field . "</span>";
								}
								$counter++;
							}
						}
						?>
					</div>
				</div>
				<?php if ($decomissioned) : ?>

				<?php else : ?>
					<div class="left-box__cta-area">
						<a class="list__button hvr-icon-forward" href="<?php echo $affiliate_url; ?>" target="_blank"><?php echo __('Register', 'casino-listings'); ?><i class="fas fa-angle-double-right hvr-icon"></i></a>
					</div>
				<?php endif; ?>
			</div>

			<div class="upper-center-box">
				<div class="center-box__wrap">
					<table id="general-info">
						<div class="casino-info-header"><i class='fas fa-info-circle'></i>

							<?php echo __('General Information', 'casino-listings'); ?>
							<?php

							if ($players_accepted) {
								foreach ($players_accepted as $flag) {
									echo "<div class='casino-flags-bottom'>";
									echo "<img src=" . CASINO_LISTINGS_PLUGIN_URL . "public/img/flags/$flag.svg' class='flag-{$flag}' alt='{$flag}'>";
									echo "<div class='toolbar {$flag}'>";
									echo __('Accepting players from ', 'casino-listings');
									echo "<span class='uppercase'>{$flag}</span></div>";
									echo "</div>";
								}
							}

							?>
						</div>
						<?php

						$icons_array = ['📅', '📋', '📩', '💶', '⌛'];
						$counter = 0;

						foreach ($general_array as $key => $value) {
							echo "<tr>" . "<td class='left'>$icons_array[$counter] $key</td>" . "<td class='right'>$value</td>" . "</tr>";
							$counter++;
						}
						?>
					</table>
				</div>
			</div>

			<div class="upper-right-box">
				<div class="center-box-right">
					<table id="bonus-info">
						<caption class="casino-info-header"><i class='fas fa-money'></i><?php echo __('Bonus information', 'casino-listings'); ?></caption>
						<?php

						$icons_array = ['💰', '💱', '✖️', '🎰', '📜'];
						$counter = 0;

						foreach ($bonus_array as $key => $value) {
							if ($counter == 0 || $counter == 4) {
								echo "<tr>" . "<td class='left'>$icons_array[$counter]$key</td>" . "<td class='right'><a class='table-link' href='{$affiliate_url}'>$value</a></td>" . "</tr>";
							} else {
								echo "<tr>" . "<td class='left'>$icons_array[$counter]$key</td>" . "<td class='right'>$value</td>" . "</tr>";
							}
							$counter++;
						}
						?>
					</table>
				</div>
			</div>
		</div>

		<div class="main-low-part">
			<div class="casino-lower-part">
				<div class="bottom-part__wrap">
					<div class="bottom-part__wrap-left">
						<div class="casino-ratings-header">
							<?php echo "<i class='fas fa-star-review'></i>" . __('Ratings', 'casino-listings'); ?>
						</div>
						<div class="casino-ratings-wrap">
							<div class="ratings-left">
								<?php

								foreach ($ratings_array as $key => $value) {
									echo "<div class='ratings-info'>{$key}</div>";
									echo "<div class='ratings-stars'>";

									for ($i = 0; $i < $maximum_rating; $i++) {
										if ($i < $value) {
											echo '<span class="star-active">★</span>';
										} else {
											echo '<span>★</span>';
										}
									}

									echo "</div>";
								}

								?>
							</div>
						</div>

					</div>
					<div class="bottom-part__wrap-right">
						<div class="casino-ratings-header">
							<?php echo "<i class='fas fa-folder-plus'></i>" . __('Features', 'casino-listings'); ?>
						</div>
						<div class="casino-info-wrap">
							<div class="info-wrap-left">

								<?php if ($casino_mobile) : ?>
									<div class="info-casino-info-left"><?php echo __('Mobile', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-check-circle"></i></div>
								<?php else : ?>
									<div class="info-casino-info-left"><?php echo __('Mobile', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-times-circle"></i></div>
								<?php endif; ?>

								<?php if (get_post_meta(get_the_ID(), 'cl_feature_live_casino', true)) : ?>
									<div class="info-casino-info-left"><?php echo __('Live Casino', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-check-circle"></i></div>
								<?php else : ?>
									<div class="info-casino-info-left"><?php echo __('Live Casino', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-times-circle"></i></div>
								<?php endif; ?>

								<div class="info-casino-info-left">
									<?php echo get_post_meta(get_the_ID(), 'cl_feature_language', true); ?>
								</div>
								<?php if (get_post_meta(get_the_ID(), 'cl_feature_lang_available', true)) : ?>
									<div class="info-casino-info-right"><i class="far fa-check-circle"></i></div>
								<?php else : ?>
									<div class="info-casino-info-right"><i class="far fa-times-circle"></i></div>
								<?php endif; ?>

								<?php if (get_post_meta(get_the_ID(), 'cl_feature_tax', true)) : ?>
									<div class="info-casino-info-left"><?php echo __('Tax', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-check-circle"></i></div>
								<?php else : ?>
									<div class="info-casino-info-left"><?php echo __('Tax', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-times-circle"></i></div>
								<?php endif; ?>

							</div>
							<div class="info-wrap-right">

								<?php if (get_post_meta(get_the_ID(), 'cl_feature_betting', true)) : ?>
									<div class="info-casino-info-left"><?php echo __('Betting', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-check-circle"></i></div>
								<?php else : ?>
									<div class="info-casino-info-left"><?php echo __('Betting', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-times-circle"></i></div>
								<?php endif; ?>

								<?php if (get_post_meta(get_the_ID(), 'cl_feature_bingo', true)) : ?>
									<div class="info-casino-info-left"><?php echo __('Bingo', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-check-circle"></i></div>
								<?php else : ?>
									<div class="info-casino-info-left"><?php echo __('Bingo', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-times-circle"></i></div>
								<?php endif; ?>

								<?php if (get_post_meta(get_the_ID(), 'cl_feature_scratch_cards', true)) : ?>
									<div class="info-casino-info-left"><?php echo __('Scratch Cards', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-check-circle"></i></div>
								<?php else : ?>
									<div class="info-casino-info-left"><?php echo __('Scratch cards', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-times-circle"></i></div>
								<?php endif; ?>

								<?php if (get_post_meta(get_the_ID(), 'cl_feature_poker', true)) : ?>
									<div class="info-casino-info-left"><?php echo __('Poker', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-check-circle"></i></div>
								<?php else : ?>
									<div class="info-casino-info-left"><?php echo __('Poker', 'casino-listings'); ?></div>
									<div class="info-casino-info-right"><i class="far fa-times-circle"></i></div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="providers-payment">
			<div class="payment-wrap">
				<div class="providers-payment-heading">
					<?php echo "<i class='fas fa-credit-card'></i>" . __('Payment Options', 'casino-listings'); ?>
				</div>
				<div class="casino-payment-methods">
					<?php
					if ($deposit_methods) {
						foreach ($deposit_methods as $method) {
							if ($method) {
								echo "<div class='review-payment-card'><img class='$method' title='$method' src=" . CASINO_LISTINGS_PLUGIN_URL . "public/img/$method.jpg alt='$method'></div>";
							}
						}
					}
					?>
					<?php if (is_array($deposit_methods) && sizeof($deposit_methods) > 5) {
						echo '<div class="show-all-payment">';
						echo __("Show all", "casino-listings");
						echo '<i class="fas fa-angle-double-right hvr-icon"></i></div>';
					}; ?>
				</div>
			</div>

			<div class="providers-wrap">
				<div class="providers-payment-heading">
					<?php echo "<i class='fas fa-cube'></i>" . __('Gaming Providers', 'casino-listings'); ?>
				</div>
				<div class="casino-providers">
					<?php
					if ($software) {
						foreach ($software as $item) {
							if ($item) {
								echo "<div class='software-item'><img class='$item' title='$item' src=" . CASINO_LISTINGS_PLUGIN_URL . "public/img/providers/$item.png alt='$item'></div>";
							}
						}
					}
					?>
					<?php if (is_array($software) && sizeof($software) > 5) {
						echo '<div class="show-all-software">';
						echo __("Show all", "casino-listings");
						echo '<i class="fas fa-angle-double-right hvr-icon"></i></div>';
					}; ?>

				</div>
			</div>
		</div>
	</div>
	<div class="casino-text-part">
		<div class="casino-text-wrap">
			<div id="casino-content">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>