<?php

if (!defined('ABSPATH')) {
    die;
}

/**
 * The template for displaying casino posts
 */

get_header();

?>

		<?php
        // Start the loop.
        while (have_posts()) :
            the_post();
            
            include_once 'single/content-casino-listing.php';

        endwhile;
        ?>
<?php get_footer(); ?>