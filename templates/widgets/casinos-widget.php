<?php

include CASINO_LISTINGS_BASE_DIR . 'templates/casino-meta-data.php';

?>


<div class="single-casino">
    <div class="single-casino_logo">
        <a href="<?php echo $affiliate_url; ?>"><img src="<?php echo $casino_image; ?>" alt=""></a>
    </div>
    <div class="single-casino_info">
        <div class="single-casino_info--bonus"><?php echo $box_bonus_field[0] . ' <span class="widget-bonus-field">' . $box_bonus_field[1] . '</span> ' . $box_bonus_field[2] ;?></div>
    </div>
    <div class="single-casino_cta"><a href="<?php echo $affiliate_url;?>" target="_blank"><?php echo __('Get Bonus', 'casino-listings'); ?></a></div>
</div>