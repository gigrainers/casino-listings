<?php 

if (!defined('ABSPATH')) {
	die;
}

/**
 * Global Settings
 */

$image_id = get_post_thumbnail_id();

/**
 * OPTIONS
 */
$terms_conditions = get_post_meta(get_the_ID(), 'cl_terms_text', true);


/**
 * More Information Dropdown
 */
$decomissioned = get_post_meta(get_the_ID(), 'cl_decomissioned', true);

$casino_launched = get_post_meta(get_the_ID(), 'cl_information_launched', true);

$casino_livechat = get_post_meta( get_the_ID(), 'cl_information_livechat', true);

$casino_mobile = get_post_meta( get_the_ID(), 'cl_information_mobile', true);

$casino_min_deposit = get_post_meta( get_the_ID(), 'cl_information_min_deposit', true);

$casino_rating = get_post_meta( get_the_ID(), 'cl_information_casino_rating', true);

$casino_introduction = get_post_meta( get_the_ID(), 'cl_information_casino_introduction', true);

// ss
$casino_bonus = get_post_meta( get_the_ID(), 'cl_information_casino_bonus', true);

$bonus_percentage = get_post_meta( get_the_ID(), 'cl_information_bonus_percentage', true);

$wager_requirements = get_post_meta( get_the_ID(), 'cl_information_wager_requirements', true);

$free_spins = get_post_meta( get_the_ID(), 'cl_information_free_spins', true);

/**
 * Image / Casino Title
 */
$casino_image = get_the_post_thumbnail_url();

$casino_title = get_the_title();


/**
 * URLS
 */
$affiliate_url = get_post_meta(get_the_ID(), 'cl_affiliate_url', true);

$casino_review_url = get_post_meta(get_the_ID(), 'cl_review_url', true);


/**
 * Data for TOPLIST style
 */
$list_bonus_field = get_post_meta(get_the_ID(), 'cl_list_bonus_field', true);

$casino_highlight = get_post_meta(get_the_ID(), 'cl_list_highlight', true);

$box_bonus_field = get_post_meta(get_the_ID(), 'cl_box_bonus_field', true);

$deposit_methods = get_post_meta(get_the_ID(), 'cl_deposit_methods', true);


/**
 * Banner info
 * 
 */

$banner_text = get_post_meta(get_the_ID(), 'cl_banner_text', true);

$banner_color = get_post_meta(get_the_ID(), 'cl_banner_color', true);

//Software
$software = get_post_meta(get_the_ID(), 'cl_software', true);

//Players accepted

$players_accepted = get_post_meta(get_the_ID(), 'cl_players_accepted', true);