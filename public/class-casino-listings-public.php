<?php

if (!defined('ABSPATH')) {
	die;
}

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://yoursite.lv
 * @since      1.0.0
 *
 * @package    Casino_Listings
 * @subpackage Casino_Listings/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Casino_Listings
 * @subpackage Casino_Listings/public
 * @author     Kristaps Ritins <kristaps@yoursite.lv>
 */

if (!class_exists('Casino_Listings_Public')) :
	class Casino_Listings_Public
	{

		/**
		 * The ID of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $plugin_name    The ID of this plugin.
		 */
		private $plugin_name;

		/**
		 * The version of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $version    The current version of this plugin.
		 */
		private $version;

		/**
		 * Initialize the class and set its properties.
		 *
		 * @since    1.0.0
		 * @param      string    $plugin_name       The name of the plugin.
		 * @param      string    $version    The version of this plugin.
		 */
		public function __construct($plugin_name, $version)
		{

			$this->plugin_name = $plugin_name;
			$this->version = $version;
		}

		/**
		 * Register the stylesheets for the public-facing side of the site.
		 *
		 * @since    1.0.0
		 */
		public function enqueue_styles()
		{

			/**
			 * An instance of this class should be passed to the run() function
			 * defined in Casino_Listings_Loader as all of the hooks are defined
			 * in that particular class.
			 *
			 * The Casino_Listings_Loader will then create the relationship
			 * between the defined hooks and the functions defined in this
			 * class.
			 */

			wp_enqueue_style('montserrat', 'https://fonts.googleapis.com/css2?family=Play:wght@400;700&display=swap');

			wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/casino-listings-public.css', array(), $this->version, 'all');

			wp_enqueue_style($this->plugin_name . '-fontawesome', CASINO_LISTINGS_PLUGIN_URL . 'vendor/fontawesome/css/all.css', array(), $this->version, 'all');

			wp_register_style(
				$this->plugin_name . '-widgets',
				plugin_dir_url(__FILE__) . 'css/casino-listings-widgets.css',
				array(),
				$this->version,
				'all'
			);


			if (is_singular('casino')) {
				wp_enqueue_style($this->plugin_name . '-single-casino', plugin_dir_url(__FILE__) . 'css/casino-listings-casino-single.css', array(), $this->version, 'all');
			}


			if (is_active_widget(
				false,
				false,
				'cl_casinos_list',
				true
			)) {
				wp_enqueue_style($this->plugin_name . '-widgets');
			}
		}

		/**
		 * Register the JavaScript for the public-facing side of the site.
		 *
		 * @since    1.0.0
		 */
		public function enqueue_scripts()
		{
			wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/casino-listings-public.js', array('jquery'), $this->version, false);

			// For load more button

			wp_enqueue_script('ajax-loadmore', plugin_dir_url(__FILE__) . 'js/myloadmore.js', array('jquery'), $this->version, false);

			wp_localize_script('ajax-loadmore', 'wp_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));

			//For ajax filters

			wp_enqueue_script('ajax', plugin_dir_url(__FILE__) . 'js/scripts.js', array('jquery'), $this->version, true);

			wp_localize_script('ajax', 'wp_ajax', array('ajax_url' => admin_url('admin-ajax.php')));

		}
	}
endif;
