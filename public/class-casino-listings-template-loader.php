<?php 

if (!defined('ABSPATH')) {
	die;
}

/**
 * Template Loader Class
 * 
 * @package Casino_Listings
 * @subpackage Casino_Listings/includes
 * @author Kristaps Ritins <kristaps@yoursite.lv>
 */

if (! class_exists('Casino_Listings_Template_Loader')) {

    if (! class_exists('Gamajo_Template_Loader')) {
        require_once CASINO_LISTINGS_BASE_DIR . 'vendor/class-gamajo-template-loader.php';
    }

    class Casino_Listings_Template_Loader extends Gamajo_Template_Loader {
        // Prefix for filter names
        protected $filter_prefix = 'casino_listings';

        //Directory name where custom templates for this plugin should be found in there.
        protected $theme_template_directory = 'casino-listings';

        //Reference to the root directory path of this plugin
        protected $plugin_directory = CASINO_LISTINGS_BASE_DIR;

        //Directory name where templates are found in this plugin
        protected $plugin_template_directory = 'templates';
    }
}
