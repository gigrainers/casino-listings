(function ($) {
    $(document).ready(function () {
        $(document).on('click', '.js-filter-item', function (e) {
            e.preventDefault();

            var category = $(this).data('category');

            $.ajax({
                url: wp_ajax.ajax_url,
                data: { action: 'filter', category: category },
                type: 'POST',
                beforeSend: function () {
                    $("#loader").show();
                    $(".cas-list-wrap").hide();
                    $(".cl-terms").hide();
                    $(".more-posts").hide();
                },

                success: function (result) {
                    $('.js-filter').html(result);
                },

                complete: function (data) {
                    $("#loader").hide();
                },

                error: function (result) {
                    console.warn(result);
                }
            });
        });
    });

})(jQuery);




