
(function ($) {
    $(document).ready(function () {
      var toplistPage = 4;    
  $(".load-more-casinos").on("click", function (e) {
    e.preventDefault();
    toplistPage++;
    const button = $('.load-more-casinos');
    $.ajax({
        method: 'POST',
        url: wp_ajax_object.ajax_url,
        data: {
        action: "casino_listing_load_more_casinos",
        page: toplistPage,
        ppp: 10,
      },

      beforeSend: function () {
        button.hide();
      },

      success: function (data) {
        $('.list-items').append(data);
        button.show();
      },

      complete: function (data) {
      },

      error: function (data) {
      }
    });
  });
});
})(jQuery);