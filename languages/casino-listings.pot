#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Casino Listings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-06-03 13:37+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.3.1; wp-5.3.2"

#: includes/class-casino-listings-post-types.php:84
msgctxt "post type general name"
msgid "Casinos"
msgstr ""

#: includes/class-casino-listings-post-types.php:85
msgctxt "post type singular name"
msgid "Casino"
msgstr ""

#: includes/class-casino-listings-post-types.php:86
msgctxt "admin menu"
msgid "Casino Listings"
msgstr ""

#: includes/class-casino-listings-post-types.php:87
msgctxt "add new on admin bar"
msgid "Casino"
msgstr ""

#: includes/class-casino-listings-post-types.php:88
msgctxt "casino"
msgid "Add New"
msgstr ""

#: includes/class-casino-listings-post-types.php:89
msgid "Add New Casino"
msgstr ""

#: includes/class-casino-listings-post-types.php:90
msgid "New Casino"
msgstr ""

#: includes/class-casino-listings-post-types.php:91
msgid "Edit Casino"
msgstr ""

#: includes/class-casino-listings-post-types.php:92
msgid "View Casino"
msgstr ""

#: includes/class-casino-listings-post-types.php:93
msgid "All Casinos"
msgstr ""

#: includes/class-casino-listings-post-types.php:94
msgid "Search Casinos"
msgstr ""

#: includes/class-casino-listings-post-types.php:95
msgid "Parent Casinos:"
msgstr ""

#: includes/class-casino-listings-post-types.php:96
msgid "No casinos found."
msgstr ""

#: includes/class-casino-listings-post-types.php:97
msgid "No casinos found in Trash."
msgstr ""

#: includes/class-casino-listings-post-types.php:98
msgid "Set Casino/Sportsbook Logo"
msgstr ""

#: includes/class-casino-listings-post-types.php:103
msgid "Description."
msgstr ""

#: includes/class-casino-listings-post-types.php:128
msgctxt "taxonomy general name"
msgid "Casino Category"
msgstr ""

#: includes/class-casino-listings-post-types.php:129
msgctxt "taxonomy singular name"
msgid "Casino Category"
msgstr ""

#: includes/class-casino-listings-post-types.php:130
msgid "Search Casino Category"
msgstr ""

#: includes/class-casino-listings-post-types.php:131
msgid "Popular Casino Category"
msgstr ""

#: includes/class-casino-listings-post-types.php:132
msgid "All Casino Category"
msgstr ""

#: includes/class-casino-listings-post-types.php:133
msgid "Parent Casino Category"
msgstr ""

#: includes/class-casino-listings-post-types.php:134
msgid "Parent Casino Category:"
msgstr ""

#: includes/class-casino-listings-post-types.php:135
msgid "Edit Casino Category"
msgstr ""

#: includes/class-casino-listings-post-types.php:136
msgid "View Casino Category"
msgstr ""

#: includes/class-casino-listings-post-types.php:137
msgid "Update Casino Category"
msgstr ""

#: includes/class-casino-listings-post-types.php:138
msgid "Add New Casino Category"
msgstr ""

#: includes/class-casino-listings-post-types.php:139
msgid "New Casino Category Name"
msgstr ""

#: includes/class-casino-listings-post-types.php:140
msgid "Separate casino category with commas"
msgstr ""

#: includes/class-casino-listings-post-types.php:141
msgid "Add or remove casino category"
msgstr ""

#: includes/class-casino-listings-post-types.php:142
msgid "Choose from the most used casino category"
msgstr ""

#: includes/class-casino-listings-post-types.php:143
msgid "No casino category found."
msgstr ""

#: includes/class-casino-listings-post-types.php:200
msgid "Toplist info"
msgstr ""

#: includes/class-casino-listings-post-types.php:207
msgid "Review URL"
msgstr ""

#: includes/class-casino-listings-post-types.php:215
msgid "Affiliate URL"
msgstr ""

#: includes/class-casino-listings-post-types.php:226
msgid "Highlight"
msgstr ""

#: includes/class-casino-listings-post-types.php:234
#: templates/single/content-casino-listing.php:168
msgid "Bonus information"
msgstr ""

#: includes/class-casino-listings-post-types.php:248
msgid "Top 3 things"
msgstr ""

#: includes/class-casino-listings-post-types.php:291
msgid "T&C's"
msgstr ""

#: includes/class-casino-listings-post-types.php:316
msgid "More Information"
msgstr ""

#: includes/class-casino-listings-post-types.php:402
msgid "Banner info"
msgstr ""

#: includes/class-casino-listings-post-types.php:432
msgid "Casino review page"
msgstr ""

#: includes/class-casino-listings-post-types.php:607
msgid "Casino schema"
msgstr ""

#: includes/class-casino-listings-shortcodes.php:119
msgid "Load more"
msgstr ""

#: includes/parts/filter-time.php:5
msgid "List updated: "
msgstr ""

#: includes/widgets/class-casino-listings-widgets-casinos-list.php:28
msgid "Casino Toplist"
msgstr ""

#: includes/widgets/class-casino-listings-widgets-casinos-list.php:85
msgid "Title:"
msgstr ""

#: includes/widgets/class-casino-listings-widgets-casinos-list.php:91
msgid "Limit:"
msgstr ""

#: templates/shortcodes/casino-list-box.php:30
#: templates/shortcodes/casino-list-info.php:96
#: templates/shortcodes/casino-list-toplist.php:96
msgid "Review"
msgstr ""

#: templates/shortcodes/casino-list-box.php:34
#: templates/shortcodes/casino-list-info.php:93
#: templates/shortcodes/casino-list-toplist.php:95
#: templates/single/content-casino-listing.php:125
msgid "Register"
msgstr ""

#: templates/shortcodes/casino-list-info.php:4
msgid "Information"
msgstr ""

#: templates/shortcodes/casino-list-info.php:9
msgid "Casino Established"
msgstr ""

#: templates/shortcodes/casino-list-info.php:16
#: templates/shortcodes/casino-list-info.php:21
msgid "Live Chat"
msgstr ""

#: templates/shortcodes/casino-list-info.php:28
#: templates/shortcodes/casino-list-info.php:33
msgid "Mobile Version"
msgstr ""

#: templates/shortcodes/casino-list-info.php:40
#: templates/shortcodes/casino-list-info.php:45
msgid "Min. Deposit"
msgstr ""

#: templates/shortcodes/casino-list-info.php:52
#: templates/shortcodes/casino-list-info.php:57
msgid "Rating"
msgstr ""

#: templates/shortcodes/casino-list-info.php:65
msgid "Introduction"
msgstr ""

#: templates/shortcodes/casino-list-info.php:69
msgid "Bonuses"
msgstr ""

#: templates/shortcodes/casino-list-info.php:73
#: templates/single/content-casino-listing.php:27
msgid "Bonus"
msgstr ""

#: templates/shortcodes/casino-list-info.php:77
msgid "Bonus Percentage"
msgstr ""

#: templates/shortcodes/casino-list-info.php:81
msgid "Rollover"
msgstr ""

#: templates/shortcodes/casino-list-info.php:85
msgid "Free Spins"
msgstr ""

#: templates/shortcodes/casino-list-toplist.php:55
msgid "Payment options:"
msgstr ""

#: templates/shortcodes/casino-list-toplist.php:74
msgid "Time for withdrawal:"
msgstr ""

#: templates/shortcodes/casino-list-toplist.php:91
msgid "Facts"
msgstr ""

#: templates/single/content-casino-listing.php:12
msgid "Bonus Offers"
msgstr ""

#: templates/single/content-casino-listing.php:13
msgid "Mobile Experience"
msgstr ""

#: templates/single/content-casino-listing.php:14
#: templates/single/content-casino-listing.php:304
msgid "Payment Options"
msgstr ""

#: templates/single/content-casino-listing.php:15
msgid "Customer Support"
msgstr ""

#: templates/single/content-casino-listing.php:19
msgid "Year of foundation"
msgstr ""

#: templates/single/content-casino-listing.php:20
msgid "License"
msgstr ""

#: templates/single/content-casino-listing.php:21
msgid "E-mail"
msgstr ""

#: templates/single/content-casino-listing.php:22
msgid "Minimum deposit"
msgstr ""

#: templates/single/content-casino-listing.php:23
msgid "Time to withdraw"
msgstr ""

#: templates/single/content-casino-listing.php:28
msgid "Bonus percentage"
msgstr ""

#: templates/single/content-casino-listing.php:29
msgid "Wager"
msgstr ""

#: templates/single/content-casino-listing.php:30
msgid "Free spins"
msgstr ""

#: templates/single/content-casino-listing.php:31
msgid "Bonus code"
msgstr ""

#: templates/single/content-casino-listing.php:44
msgid "Get your bonus"
msgstr ""

#: templates/single/content-casino-listing.php:86
msgid "Last updated: "
msgstr ""

#: templates/single/content-casino-listing.php:135
msgid "General Information"
msgstr ""

#: templates/single/content-casino-listing.php:143
msgid "Accepting players from "
msgstr ""

#: templates/single/content-casino-listing.php:193
msgid "Ratings"
msgstr ""

#: templates/single/content-casino-listing.php:221
msgid "Features"
msgstr ""

#: templates/single/content-casino-listing.php:227
#: templates/single/content-casino-listing.php:230
msgid "Mobile"
msgstr ""

#: templates/single/content-casino-listing.php:235
#: templates/single/content-casino-listing.php:238
msgid "Live Casino"
msgstr ""

#: templates/single/content-casino-listing.php:252
#: templates/single/content-casino-listing.php:255
msgid "Tax"
msgstr ""

#: templates/single/content-casino-listing.php:263
#: templates/single/content-casino-listing.php:266
msgid "Betting"
msgstr ""

#: templates/single/content-casino-listing.php:271
#: templates/single/content-casino-listing.php:274
msgid "Bingo"
msgstr ""

#: templates/single/content-casino-listing.php:279
msgid "Scratch Cards"
msgstr ""

#: templates/single/content-casino-listing.php:282
msgid "Scratch cards"
msgstr ""

#: templates/single/content-casino-listing.php:287
#: templates/single/content-casino-listing.php:290
msgid "Poker"
msgstr ""

#: templates/single/content-casino-listing.php:318
#: templates/single/content-casino-listing.php:340
msgid "Show all"
msgstr ""

#: templates/single/content-casino-listing.php:326
msgid "Gaming Providers"
msgstr ""

#: templates/widgets/casinos-widget.php:15
msgid "Get Bonus"
msgstr ""

#. Name of the plugin
msgid "Casino Listings"
msgstr ""

#. Description of the plugin
msgid "Create different casino toplists, boxes and widgets"
msgstr ""

#. URI of the plugin
#. Author URI of the plugin
msgid "https://yoursite.lv"
msgstr ""

#. Author of the plugin
msgid "Kristaps Ritins"
msgstr ""
