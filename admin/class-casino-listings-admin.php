<?php

// If this file is called directly, abort.
if (!defined('ABSPATH')) {
	die;
}

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://yoursite.lv
 * @since      1.0.0
 *
 * @package    Casino_Listings
 * @subpackage Casino_Listings/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Casino_Listings
 * @subpackage Casino_Listings/admin
 * @author     Kristaps Ritins <kristaps@yoursite.lv>
 */

if (!class_exists('Casino_Listings_Admin')) :
	class Casino_Listings_Admin
	{

		/**
		 * The ID of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $plugin_name    The ID of this plugin.
		 */
		private $plugin_name;

		/**
		 * The version of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $version    The current version of this plugin.
		 */
		private $version;

		/**
		 * Initialize the class and set its properties.
		 *
		 * @since    1.0.0
		 * @param      string    $plugin_name       The name of this plugin.
		 * @param      string    $version    The version of this plugin.
		 */
		public function __construct($plugin_name, $version)
		{

			$this->plugin_name = $plugin_name;
			$this->version = $version;
		}

		/**
		 * Register the stylesheets for the admin area.
		 *
		 * @since    1.0.0
		 */
		public function enqueue_styles()
		{
			wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/casino-listings-admin.css', array(), $this->version, 'all');
		}

		/**
		 * Register the JavaScript for the admin area.
		 *
		 * @since    1.0.0
		 */
		public function enqueue_scripts()
		{
			wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/casino-listings-admin.js', array('jquery'), $this->version, false);
		}

		/**
		 * Add admin menu for our plugin
		 */

		public function add_admin_menu()
		{
			add_submenu_page(
				'edit.php?post_type=casino-listing',
				'Schema',
				'Settings',
				'manage_options',
				'schema-info',
				array($this, 'schema_page_display')
			);

			//Submenu page
			add_submenu_page(
				'edit.php?post_type=casino-listing',
				'Shortcodes',
				'Help',
				'manage_options',
				'casino-listings',
				array($this, 'admin_page_display')
			);
		}


		public function schema_page_display()
		{
			include 'partials/casino-listings-admin-display-form.php';
		}

		/**
		 * Admin page display
		 */
		public function admin_page_display()
		{
			include 'partials/shortcode-help.php';
		}


		/**
		 * All hooks for admin_init
		 */
		public function admin_init()
		{
			//Add Settings Section
			// $this->add_settings_section();


			//Add Settings Fields
			// $this->add_settings_fields();


			//Save Settings
			// $this->save_fields();
		}



		/**
		 * Add Settings Sections for plugin options
		 */
		public function add_settings_section()
		{
			add_settings_section(
				'cl-general-section',
				'General Settings',
				function () {
					echo '<p>These are general settings for Casino Listings plugin</p>';
				},
				'cl-settings-page'
			);
		}

		/**
		 * Add setting fields
		 *
		 */
		public function add_settings_fields()
		{
			add_settings_field(
				'cl_site_name',
				'Site name',
				array($this, 'cl_site_name'),
				'cl-settings-page',
				'cl-general-section',
				array(
					'name' => 'cl_site_name',
					'value' => get_option('cl_site_name')
				)
			);
			
		}

		/**
		 * Save settings fields
		 */
		public function save_fields()
		{
			register_setting(
				'cl-schema-fields',
				'cl_site_name'
			);
		}


		/**
		 * Markup for text fields
		 */
		public function markup_color_field_cb($args)
		{
			/*
			if (!is_array($args)) {
				return null;
			}

			$name = (isset($args['name'])) ? esc_html($args['name']) : '';

			$value = (isset($args['value'])) ? esc_html($args['value']) : '';

?>

			<input type="color" name="<?php echo $name; ?>" class="field-<?php echo $name; ?>" value="<?php echo $value; ?>">

		<?php

		*/
		}
	}
endif;
