<?php

if (!defined('ABSPATH')) {
    die;
}

?>



<div class="help-container">
    <div class="help-wrap">
        <h1><?php echo get_admin_page_title() . " - [casino_list]"; ?></h1>
        <h2>Limit</h2>
        <p><strong>[casino_list limit=10]</strong></p>
        <p>Will output maximum 10 casinos</p>
        <p>DEFAULT VALUE: Settings -> Reading -> Blog pages show at most</p>
        <hr>
        <h2>ID</h2>
        <strong>[casino_list ID=21]</strong>
        <p>Will output casino with ID=21</p>
        <strong>[casino_list ID=1,2,3,4]</strong>
        <p>Will output multiple casinos with given ID's</p>
        <hr>
        <h2>Time</h2>
        <strong>[casino_list time=on]</strong>
        <p>Will output casino list with list updated feature at the top of the list</p>
        <p>DEFAULT VALUE: off</p>
        <hr>
        <h2>Ajax filter</h2>
        <p>[casino_list filter=on]</p>
        <p>Will enable AJAX filter. Categories can be created at Casinos -> Casino Category. These categories will be displayed above the toplist if filtering is enabled</p>
        <p>DEFAULT VALUE: off</p>
        <hr>
        <h2>Single list item</h2>
        <strong>[casino_list single=on id=10]</strong>
        <p>Will turn on style for single page casino toplist with no order</p>
        <p>DEFAULT VALUE: off</p>
        <hr>
        <h2>Shortcode attributes can be combined</h2>
        <strong>[casino_list id=10 time=on single=on]</strong>
        <p>Can use with multiple attributes</p>
        <hr>
        <h1><?php echo "Shortcodes - [casino_box]"; ?></h1>
        <h2>Limit</h2>
        <p><strong>[casino_box limit=10]</strong></p>
        <p>Will output maximum 10 casinos</p>
        <p>DEFAULT VALUE: Settings -> Reading -> Blog pages show at most</p>
        <hr>
        <h2>ID</h2>
        <strong>[casino_list ID=21]</strong>
        <p>Will output casino with ID=21</p>
        <strong>[casino_list ID=1,2,3,4]</strong>
        <p>Will output multiple casinos with given ID's</p>
        <hr>
        <h2>Style</h2>
        <strong>[casino_list style=wide]</strong>
        <p>Will output casino with wide style</p>
        <hr>
    </div>
</div>