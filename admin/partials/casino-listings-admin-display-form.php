<?php

if (!defined('ABSPATH')) {
	die;
}

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://yoursite.lv
 * @since      1.0.0
 *
 * @package    Casino_Listings
 * @subpackage Casino_Listings/admin/partials
 */

if (isset($_POST) && !empty($_POST)) {

	update_option('cl_site_name', $_POST['cl-site-name']);

	update_option('cl_schema_enabled', $_POST['cl-schema-enabled']);

	update_option('cl_btn_color_1', $_POST['cl-btn-color-1']);

	update_option('cl_btn_color_2', $_POST['cl-btn-color-2']);

	update_option('cl_btn_color_3', $_POST['cl-btn-color-3']);

	update_option('cl_btn_color_4', $_POST['cl-btn-color-4']);
}

?>


<div class="wrap">
	<h1>General settings</h1>

	<form method="post" action="">
		<input type="hidden" name="option_page" value="general"><input type="hidden" name="action" value="update"><input type="hidden" id="_wpnonce" name="_wpnonce" value="46dc34f193"><input type="hidden" name="_wp_http_referer" value="/wp-admin/options-general.php">
		<table class="form-table" role="presentation">
			<tbody>
				<tr>
					<th scope="row"><label for="schema_enabled">Schema enabled</label></th>
					<td><input name="cl-schema-enabled" type="checkbox" id="cl-schema-enabled" value="1" <?php checked('1', get_option('cl_schema_enabled')); ?>></td>
				</tr>
				<tr>
					<th scope="row"><label for="site_name">Site name</label></th>
					<td><input name="cl-site-name" type="text" id="cl-site-name" value="<?php echo get_option('cl_site_name'); ?>" class="short-text"></td>
				</tr>
				<tr>
					<th scope="row"><label for="btn_color_1">Toplist register button color</label></th>
					<td><input name="cl-btn-color-1" type="text" id="cl-btn-color-1" value="<?php echo get_option('cl_btn_color_1'); ?>" class="short-text"></td>
				</tr>
				<tr>
					<th scope="row"><label for="btn_color_2">More info register button color</label></th>
					<td><input name="cl-btn-color-2" type="text" id="cl-btn-color-2" value="<?php echo get_option('cl_btn_color_2'); ?>" class="short-text"></td>
				</tr>
				<tr>
					<th scope="row"><label for="btn_color_3">More info review button color</label></th>
					<td><input name="cl-btn-color-3" type="text" id="cl-btn-color-3" value="<?php echo get_option('cl_btn_color_3'); ?>" class="short-text"></td>
				</tr>
				<tr>
					<th scope="row"><label for="btn_color_4">Box style button color</label></th>
					<td><input name="cl-btn-color-4" type="text" id="cl-btn-color-4" value="<?php echo get_option('cl_btn_color_4'); ?>" class="short-text"></td>
				</tr>
			</tbody>
		</table>

		<?php submit_button(); ?>
	</form>

</div>