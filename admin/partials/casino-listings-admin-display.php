<?php

if (!defined('ABSPATH')) {
	die;
}

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://yoursite.lv
 * @since      1.0.0
 *
 * @package    Casino_Listings
 * @subpackage Casino_Listings/admin/partials
 */
?>

<div class="wrap">
	<h1><?php echo get_admin_page_title(); ?></h1>

	<form method="post" action="options.php">
		<?php
//Security
settings_fields('cl-settings-page-options-group');

		//Display sections
		do_settings_sections('cl-settings-page');

		?>
		<?php submit_button(); ?>
	</form>

</div>

