<div class="wrap">
	<h1><?php echo get_admin_page_title(); ?></h1>

	<form method="post" action="options.php">
		<?php
//Security
        settings_fields('cl-schema-fields');

		//Display sections
		do_settings_sections('cl-settings-page');

		?>
		<?php submit_button(); ?>
	</form>

</div>