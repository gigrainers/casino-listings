<?php

if (!defined('ABSPATH')) {
	die;
}

/**
 * Register Widget: Casinos List
 * 
 * @package Casino_Listings
 * @subpackage Casino_Listings/includes
 * @author Kristaps
 */

if (!class_exists('Casino_Listings_Widget_Casinos_List')) {
    class Casino_Listings_Widget_Casinos_List extends WP_Widget
    {

        /**
         * Sets up the widgets name etc
         */
        public function __construct()
        {
            $widget_ops = array(
                'classname' => 'cl_casinos_list_class',
                'description' => 'Display Casino Toplists',
            );
            parent::__construct('cl_casinos_list', __('Casino Toplist', 'casino-listings'), $widget_ops);
        }

        /**
         * Outputs the content of the widget
         *
         * @param array $args
         * @param array $instance
         */
        public function widget($args, $instance)
        {
            $title = isset($instance['title']) ? $instance['title'] : '';
            $limit = isset($instance['limit']) ? $instance['limit'] : 5;

            echo $args['before_widget'];
            echo $args['before_title'];
            //Title will be displayed here
            echo esc_html($title);
            echo $args['after_title'];

            //Loop for CPT Casinos
            $loop_args = array(
                'post_type' => 'casino-listing',
                'posts_per_page' => $limit,
            );

            $loop = new WP_Query($loop_args);

            echo '<div class="casinos-widget">';

            while ($loop->have_posts()) :

                $loop->the_post();

                include CASINO_LISTINGS_BASE_DIR . 'templates/widgets/casinos-widget.php';

            endwhile;

            echo '</div>';

            echo $args['after_widget'];
        }

        /**
         * Outputs the options form on admin
         *
         * @param array $instance The widget options
         */
        public function form($instance)
        {

            $title = isset($instance['title']) ? $instance['title'] : '';

            $limit = isset($instance['limit']) ? $instance['limit'] : 5;
?>

            <p>
                <label for="<?php echo esc_attr($this->get_field_name('title')); ?>"><?php _e('Title:', 'casino-listings'); ?></label>

                <input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_html($title); ?>">
            </p>

            <p>
                <label for="<?php echo esc_attr($this->get_field_name('limit')); ?>"><?php _e('Limit:', 'casino-listings'); ?></label>

                <input type="number" class="widefat" id="<?php echo esc_attr($this->get_field_id('limit')); ?>" name="<?php echo esc_attr($this->get_field_name('limit')); ?>" value="<?php echo esc_html($limit); ?>">
            </p>


<?php
            // outputs the options form on admin
        }

        /**
         * Processing widget options on save
         *
         * @param array $new_instance The new options
         * @param array $old_instance The previous options
         *
         * @return array
         */
        public function update($new_instance, $old_instance)
        {
            // processes widget options to be saved

            //Sanitization of $new_instance

            $sanitized_instance['title'] = sanitize_text_field($new_instance['title']);

            $sanitized_instance['limit'] = absint($new_instance['limit']);

            return $sanitized_instance;
        }
    }
}
