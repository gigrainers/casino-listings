<?php

if (!defined('ABSPATH')) {
    die;
}

if (get_option('cl_schema_enabled', 1)) {
    add_action(
        'wp_head',
        function () {
            $schema = array(
                "@context" => "http://schema.org",
                "@type" => "Review",
                "name" => get_the_title(),
                "itemReviewed" => array(
                    "@type" => "Organization",
                    "name" => get_the_title(),
                    "brand" => get_post_meta(get_the_ID(), 'cl_schema_brand', true),
                    "description" => get_post_meta(get_the_ID(), 'cl_schema_description', true),
                    "review" => array(
                        "@type" => "Review",
                        "name" => get_the_title(),
                        "datePublished" => get_the_date(),
                        "dateModified" => get_the_modified_date(),
                        "author" => get_option('cl_site_name')
                    ),
                    "aggregateRating" => array(
                        "@type" => "AggregateRating",
                        "ratingValue" => get_post_meta(get_the_ID(), 'cl_information_casino_rating', true),
                        "bestRating" => 5,
                        "worstRating" => 1,
                        "ratingCount" => get_post_meta(get_the_ID(), 'cl_schema_rating_count', true),
                    ),
                    "image" => array(
                        "@type" => "ImageObject",
                        "url" => get_the_post_thumbnail_url(),
                    )
                ),
                "author" => array(
                    "@type" => "Organization",
                    "name" => get_option('cl_site_name'),
                    "url" => get_site_url(),
                ),
                "publisher" => array(
                    "@type" => "Organization",
                    "name" => get_option('cl_site_name'),
                    "url" => get_site_url(),
                ),
                "image" => array(
                    "@type" => "ImageObject",
                    "url" => get_the_post_thumbnail_url(),
                )

            );
            if (get_post_type() == 'casino-listing')
                echo '<script type="application/ld+json">' . json_encode($schema) . '</script>';
        }
    );
}

?>