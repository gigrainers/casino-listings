<?php

add_action('wp_ajax_nopriv_casino_listing_load_more_casinos', 'casino_listing_load_more_casinos');
add_action('wp_ajax_casino_listing_load_more_casinos', 'casino_listing_load_more_casinos');


function casino_listing_load_more_casinos()
{

    $pageNumber = (int)$_POST["page"];            
    $page = empty( $pageNumber )? 1 : $pageNumber;
    $postPerPage = (int)$_POST["ppp"];     
    $ppp = empty($postPerPage) ? 10 : $postPerPage; 



                $loop_args = array(
                    'post_type' => 'casino-listing',
                    'orderby' => 'title',
                    'order' => 'ASC',
                    'posts_per_page' => $ppp,
                    'paged' => $page,
                );

    $loop = new WP_Query($loop_args);

    while ($loop->have_posts()) :
        $loop->the_post();
        include CASINO_LISTINGS_BASE_DIR . 'templates/shortcodes/casino-list-toplist.php';
    // End the loop.
    endwhile;

    wp_reset_postdata();

    die();
}
