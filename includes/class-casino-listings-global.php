<?php

if (!defined('ABSPATH')) {
	die;
}

/**
 * Global Shared Class for the plugin
 * 
 * @package Casino_Listings
 * @subpackage Casino_Listings/includes
 * @author Kristaps
 */

if (!class_exists('Casino_Listings_Global')) {
    class Casino_Listings_Global
    {
        protected static $template_loader;

        public static function template_loader()
        {
            if (empty(self::$template_loader)) {
                self::set_template_loader();
            }

            return self::$template_loader;
        }

        public static function set_template_loader()
        {
            //template for CPT casino
            require_once CASINO_LISTINGS_BASE_DIR . 'public/class-casino-listings-template-loader.php';

            self::$template_loader = new Casino_Listings_Template_Loader();
        }
    }
}
