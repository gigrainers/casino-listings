<?php

if (!defined('ABSPATH')) {
	die;
}

/**
 * Fired during plugin deactivation
 *
 * @link       https://yoursite.lv
 * @since      1.0.0
 *
 * @package    Casino_Listings
 * @subpackage Casino_Listings/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Casino_Listings
 * @subpackage Casino_Listings/includes
 * @author     Kristaps Ritins <kristaps@yoursite.lv>
 */

if (!class_exists('Casino_Listings_Dectivator')) :
	class Casino_Listings_Deactivator
	{

		/**
		 * Short Description. (use period)
		 *
		 * Long Description.
		 *
		 * @since    1.0.0
		 */
		public static function deactivate()
		{

			//Unregister CPT Casino
			unregister_post_type('casino');

			//Unregister Taxonomy
			unregister_taxonomy('casino_category');

			//Flush rewrite rules
			flush_rewrite_rules();
		}
	}
endif;
