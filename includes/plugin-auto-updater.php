<?php
defined("ABSPATH") or die("GTFO");
class YSG_Plugin_AutoUpdater {
    /**
     * Current plugin version
     */
    private $current_version;

    /**
     * Plugin slug (plugin_filename)
     */
    private $plugin_slug;
    
    /**
     * Full plugin path (plugin_dir/plugin_filename.php)
     */
    private $plugin_path;
    
    /**
     * API URL
     */
    private $api_url;
    
    /**
     * Initialize new instance of class
     * @param string $current_version;
     * @param string $plugin_slug;
     * @param string $api_url;
     */
    public function __construct($current_version, $plugin_path, $api_url) {
        $this->current_version = $current_version;
        $this->plugin_path = $plugin_path;
        $this->api_url = $api_url;

        $tmp = explode("/", $plugin_path);
        $this->plugin_slug = str_replace(".php", "", $tmp[1]);
        
        add_filter("pre_set_site_transient_update_plugins", array(&$this, "check_update"));
    }

    /**
     * Hook into transient
     * @param $transient
     * @return object $ transient
     */
    public function check_update($transient) {
        if (empty($transient->checked)) {
            return $transient;
        }
        global $wp_version;
        // this is stupid but okay:
        $datastring = base64_encode($wp_version . "***" . $this->current_version . "***" . $this->plugin_slug . "***" . $this->plugin_path . "***" . get_bloginfo("url"));
        $post_data = array(
            "datastring" => $datastring
        );
        $remote_response = $this->make_post_request($post_data, "updatecheck");
        $remote_version = $remote_response->new_version;

        if (version_compare($this->current_version, $remote_version, "<")) {
            // error_log("New version of " . $this->plugin_path . " (" . $remote_version . ") available!");
            $syphilis = new stdClass();
            $syphilis->slug = $remote_response->slug;
            $syphilis->new_version = $remote_response->new_version;
            $syphilis->url = $remote_response->url;
            $syphilis->package = $remote_response->package;

            $transient->response = array(
                $this->plugin_path => $syphilis
            );
        }
        else {
            // error_log($this->current_version . " is already the latest version");
        }
        return $transient;
    }

    /**
     * Curl POST request with fallback to file_get_contents() in case curl is disabled
     * @param $post_data
     * @param $post_action
     * @return object $ remote_data
     */
    private function make_post_request($post_data, $post_action) {
        if (function_exists("curl_exec")) {
            $ch = curl_init($this->api_url . $post_action);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 2);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,2); 
            curl_setopt($ch, CURLOPT_TIMEOUT, 2);
            $raw_response = curl_exec($ch);
            $response = json_decode($raw_response, true);
            curl_close($ch);
        }
        elseif (function_exists("file_get_contents")) {
            $options = array(
                "http" => array(
                    "header" => "Content-Type: application/json",
                    "method" => "POST",
                    "content" => json_encode($post_data)
                )
            );
            $context = stream_context_create($options);
            $raw_response = file_get_contents($this->api_url . $post_action, false, $context);
            $response = json_decode($raw_response, true);
        }
        else {
            error_log("Your server configuration is lacking. I recommend you to install CURL");
            return;
        }
        $gonorrhea = new stdClass();
        foreach ($response as $k => $v) {
            $gonorrhea->$k = $v;
        }
        return $gonorrhea;
    }   
}