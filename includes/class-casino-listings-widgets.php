<?php

if (!defined('ABSPATH')) {
	die;
}

/**
 * Controlling all widgets for plugin
 * 
 * @package Casino_Listings
 * @subpackage Casino_Listings/includes
 * @author Kristaps
 */

if (!class_exists('Casino_Listings_Widgets')) {

    class Casino_Listings_Widgets
    {
        private $plugin_name;

        private $version;

        public function __construct($plugin_name, $version)
        {

            $this->plugin_name = $plugin_name;
            $this->version = $version;

            $this->load_dependency();
        }

        /**
         * Load Widget class dependencies
         */
        public function load_dependency()
        {

            require_once CASINO_LISTINGS_BASE_DIR . 'vendor/boo-widget-helper/class-boo-widget-helper.php';
        }

        /**
         * Register widgets for the plugin
         */
        public function register_widgets()
        {

            require_once CASINO_LISTINGS_BASE_DIR . 'includes/widgets/class-casino-listings-widgets-casinos-list.php';

            register_widget('Casino_Listings_Widget_Casinos_List');
        }
    }
}
