<?php

if (!defined('ABSPATH')) {
    die;
}

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://yoursite.lv
 * @since      1.0.0
 *
 * @package    Casino_Listings
 * @subpackage Casino_Listings/public
 */

/**
 * Functionality for Custom Post types
 *
 *
 * @package    Casino_Listings
 * @subpackage Casino_Listings/public
 * @author     Kristaps Ritins <kristaps@yoursite.lv>
 */
if (!class_exists('Casino_Listings_Post_Types')) :
    class Casino_Listings_Post_Types
    {

        /**
         * The ID of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string    $plugin_name    The ID of this plugin.
         */
        private $plugin_name;

        /**
         * The version of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string    $version    The current version of this plugin.
         */
        private $version;

        private $template_loader;

        /**
         * Initialize the class and set its properties.
         *
         * @since    1.0.0
         * @param      string    $plugin_name       The name of the plugin.
         * @param      string    $version    The version of this plugin.
         */
        public function __construct($plugin_name, $version)
        {

            $this->plugin_name = $plugin_name;
            $this->version = $version;

            $this->template_loader = cl_get_template_loader();
        }

        /**
         * Hooked into 'init' action hook
         */

        public function init()
        {
            $this->register_cpt_casino();

            $this->register_taxonomy_category();
        }


        /**
         * Register custom Casino post type
         */

        public function register_cpt_casino()
        {
            $labels = array(
                'name'               => _x('Casinos', 'post type general name', 'casino-listings'),
                'singular_name'      => _x('Casino', 'post type singular name', 'casino-listings'),
                'menu_name'          => _x('Casino Listings', 'admin menu', 'casino-listings'),
                'name_admin_bar'     => _x('Casino', 'add new on admin bar', 'casino-listings'),
                'add_new'            => _x('Add New', 'casino', 'casino-listings'),
                'add_new_item'       => __('Add New Casino', 'casino-listings'),
                'new_item'           => __('New Casino', 'casino-listings'),
                'edit_item'          => __('Edit Casino', 'casino-listings'),
                'view_item'          => __('View Casino', 'casino-listings'),
                'all_items'          => __('All Casinos', 'casino-listings'),
                'search_items'       => __('Search Casinos', 'casino-listings'),
                'parent_item_colon'  => __('Parent Casinos:', 'casino-listings'),
                'not_found'          => __('No casinos found.', 'casino-listings'),
                'not_found_in_trash' => __('No casinos found in Trash.', 'casino-listings'),
                'featured_image'     => __('Set Casino/Sportsbook Logo', 'casino-listings')
            );

            $args = array(
                'labels'             => $labels,
                'description'        => __('Description.', 'casino-listings'),
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                // 'rewrite'            => array('slug' => 'casino'),
                'rewrite'            => array('rewrite' => false),
                'capability_type'    => 'post',
                'has_archive'        => false,
                'taxonomies'         => array('casino-category'),
                'menu_position'      => 1,
                'menu_icon'          => 'dashicons-star-half',
                'supports'           => array('title', 'thumbnail', 'editor', 'author')

            );

            register_post_type('casino-listing', $args);
        }

        public function register_taxonomy_category()
        {
            register_taxonomy('casino-category', array('casino-listing'), array(
                'description'       => 'Casino Category',
                'labels'            => array(
                    'name'                       => _x('Casino Category', 'taxonomy general name', 'casino-listings'),
                    'singular_name'              => _x('Casino Category', 'taxonomy singular name', 'casino-listings'),
                    'search_items'               => __('Search Casino Category', 'casino-listings'),
                    'popular_items'              => __('Popular Casino Category', 'casino-listings'),
                    'all_items'                  => __('All Casino Category', 'casino-listings'),
                    'parent_item'                => __('Parent Casino Category', 'casino-listings'),
                    'parent_item_colon'          => __('Parent Casino Category:', 'casino-listings'),
                    'edit_item'                  => __('Edit Casino Category', 'casino-listings'),
                    'view_item'                  => __('View Casino Category', 'casino-listings'),
                    'update_item'                => __('Update Casino Category', 'casino-listings'),
                    'add_new_item'               => __('Add New Casino Category', 'casino-listings'),
                    'new_item_name'              => __('New Casino Category Name', 'casino-listings'),
                    'separate_items_with_commas' => __('Separate casino category with commas', 'casino-listings'),
                    'add_or_remove_items'        => __('Add or remove casino category', 'casino-listings'),
                    'choose_from_most_used'      => __('Choose from the most used casino category', 'casino-listings'),
                    'not_found'                  => __('No casino category found.', 'casino-listings'),
                ),
                'public'            => false,
                'show_ui'           => true,
                'show_in_nav_menus' => true,
                'show_admin_column' => true,
                'rewrite'           => false,
                'capabilities'      => array(),
                'show_in_rest'      => true,
            ));
        }


        /**
         * Single template for CPT Casino
         */

        public function single_template_casino($template)
        {

            if (is_singular('casino-listing')) {

                //template for CPT casino

                return $this->template_loader->get_template_part('single', 'casino-listing', false);
            }

            return $template;
        }

        /**
         * Archive template for CPT Casino
         */

        public function archive_template_casino($template)
        {

            if (cl_is_archive_casino()) {

                //template for CPT casino

                return $this->template_loader->get_template_part('archive', 'casino-listing', false);
            }

            return $template;
        }

        /**
         * Adding Metaboxes using CMB2 framework
         */
        public function register_cmb2_metabox_casino()
        {
            /**
             * Creating METABOX for LIST layout
             */
            $casinos_list_layout = new_cmb2_box(array(
                'id' => 'list-layout-data',
                'title' => __('Toplist info', 'casino-listings'),
                'object_types' => array('casino-listing'),
                'context' => 'normal'
            ));

            $casinos_list_layout->add_field(array(
                'id' => 'cl_affiliate_url',
                'name' => __('Affiliate URL', 'casino-listings'),
                'desc' => 'Casino Affiliate URL for example - /go/casino-rizk',
                'type' => 'text',

            ));

            $casinos_list_layout->add_field(array(
                'name' => 'Decomissioned Casino',
                'id'   => 'cl_decomissioned',
                'type' => 'checkbox',
            ));

            /**
             * Highlight about this casino
             */
            $casinos_list_layout->add_field(array(
                'id' => 'cl_list_highlight',
                'name' => __('Highlight', 'casino-listings'),
                'desc' => 'Highlight text about this casino',
                'type' => 'text_medium',
            ));


            $casinos_list_layout->add_field(array(
                'id' => 'cl_box_bonus_field',
                'name' => __('Bonus information', 'casino-listings'),
                'desc' => 'Casino bonus 3 rows(second row for amount)',
                'repeatable' => true,
                'type' => 'text',
                'text' => array(
                    'add_row_text' => 'Add',
                )
            ));

            /**
             * Top 3 things about this casino
             */
            $casinos_list_layout->add_field(array(
                'id' => 'cl_list_bonus_field',
                'name' => __('Top 3 things', 'casino-listings'),
                'desc' => 'Top 3 things for given casino',
                'repeatable' => true,
                'type' => 'text',
                'text' => array(
                    'add_row_text' => 'Add',
                )
            ));


            /**
             * Deposit methods
             */
            $casinos_list_layout->add_field(array(
                'name'    => 'Deposit methods',
                'desc'    => 'Types of deposit methods',
                'id'      => 'cl_deposit_methods',
                'type'    => 'multicheck_inline',
                'options' => array(
                    'visa' => 'VISA',
                    'mastercard' => 'Mastercard',
                    'skrill' => 'Skrill',
                    'neteller' => 'Neteller',
                    'swedbank' => 'Swedbank',
                    'seb' => 'SEB',
                    'citadele' => 'Citadele',
                    'nordea' => 'Nordea',
                    'ecopayz' => 'EcoPayz',
                    'bitcoin' => 'Bitcoin',
                    'astropay' => 'AstroPay',
                    'trustly'  => 'Trustly',
                    'paysafe'  => 'Paysafe',
                    'applepay' => 'Apple Pay',
                    'swish' => 'Swish',
                    'paypal' => 'Paypal',
                    'brite' => 'Brite'
                ),
            ));

            /**
             * ----------------------------------------------------------------------------------------- Terms and Conditions
             */
            $casinos_options = new_cmb2_box(array(
                'id' => 'options-data',
                'title' => __('T&C\'s', 'casino-listings'),
                'object_types' => array('casino-listing'),
                'context' => 'normal'
            ));

            $casinos_options->add_field(array(
                'name' => 'Terms and Conditions',
                'desc' => 'Enable T&Cs field',
                'id'   => 'cl_terms',
                'type' => 'checkbox',
            ));

            $casinos_options->add_field(array(
                'name' => 'Text',
                'desc' => 'Terms and conditions text',
                'id'   => 'cl_terms_text',
                'type' => 'text',
            ));


            /**
             * ----------------------------------------------------------------------------------------- Dropdown Info
             */
            $casinos_more_information = new_cmb2_box(array(
                'id' => 'more-information-data',
                'title' => __('More Information', 'casino-listings'),
                'object_types' => array('casino-listing'),
                'context' => 'normal'
            ));

            // $casinos_more_information->add_field(array(
            //     'name'             => 'Currency',
            //     'desc'             => 'Select a currency',
            //     'id'               => 'cl_info_currency',
            //     'type'             => 'select',
            //     'show_option_none' => true,
            //     'default'          => 'custom',
            //     'options'          => array(
            //         '€' => __('Euro', 'cmb2'),
            //         '$'   => __('Dollar', 'cmb2'),
            //         '£'     => __('Pound', 'cmb2'),
            //         'kr'     => __('Swedish krona', 'cmb2'),
            //     ),
            // ));

            $casinos_more_information->add_field(array(
                'name' => 'Live chat available?',
                'id'   => 'cl_information_livechat',
                'type' => 'checkbox',
            ));

            $casinos_more_information->add_field(array(
                'name' => 'Mobile version available?',
                'id'   => 'cl_information_mobile',
                'type' => 'checkbox',
            ));

            $casinos_more_information->add_field(array(
                'name' => 'Casino launch year',
                'id'   => 'cl_information_launched',
                'type' => 'text_small',
            ));

            $casinos_more_information->add_field(array(
                'name' => 'Minimal deposit amount',
                'id'   => 'cl_information_min_deposit',
                'type' => 'text_small',
            ));

            $casinos_more_information->add_field(array(
                'name' => 'Casino rating(up to 5)',
                'id'   => 'cl_information_casino_rating',
                'type' => 'text_small',
            ));

            $casinos_more_information->add_field(array(
                'name' => 'Casino Bonus',
                'id'   => 'cl_information_casino_bonus',
                'type' => 'text_small',
            ));

            $casinos_more_information->add_field(array(
                'name' => 'Bonus Percentage',
                'id'   => 'cl_information_bonus_percentage',
                'type' => 'text_small',
            ));

            $casinos_more_information->add_field(array(
                'name' => 'Wager Requirements',
                'id'   => 'cl_information_wager_requirements',
                'type' => 'text_small',
            ));

            $casinos_more_information->add_field(array(
                'name' => 'Free Spins',
                'id'   => 'cl_information_free_spins',
                'type' => 'text_small',
            ));

            $casinos_more_information->add_field(array(
                'name' => 'Casino introduction',
                'desc' => 'Short information about casino',
                'id'   => 'cl_information_casino_introduction',
                'type' => 'textarea',
            ));

            /**
             * ----------------------------------------------------------------------------------------- Banner Info
             */
            $casinos_banner = new_cmb2_box(array(
                'id' => 'banner-data',
                'title' => __('Banner info', 'casino-listings'),
                'object_types' => array('casino-listing'),
                'context' => 'normal'
            ));

            $casinos_banner->add_field(array(
                'name' => 'Enable Banner',
                'desc' => 'Enable banner for casino list',
                'id'   => 'cl_banner',
                'type' => 'checkbox',
            ));

            $casinos_banner->add_field(array(
                'name' => 'Banner text',
                'desc' => 'Banner information text',
                'id'   => 'cl_banner_text',
                'type' => 'text',
            ));

            $casinos_banner->add_field(array(
                'name' => 'Banner color',
                'desc' => 'Pick HEX value e.g #AA4F3C - https://htmlcolorcodes.com/color-picker/',
                'id'   => 'cl_banner_color',
                'type' => 'text',
            ));


            // ----------------------------------------------------------------------------------------- Review Page
            $casinos_review = new_cmb2_box(array(
                'id' => 'review-page-data',
                'title' => __('Casino review page', 'casino-listings'),
                'object_types' => array('casino-listing'),
                'context' => 'normal'
            ));

            $casinos_review->add_field(array(
                'name' => 'Casino licence',
                'id'   => 'cl_review_licence',
                'type' => 'text',
            ));

            $casinos_review->add_field(array(
                'name' => 'Casino e-mail',
                'id'   => 'cl_review_email',
                'type' => 'text',
            ));

            $casinos_review->add_field(array(
                'name' => 'Casino bonus code',
                'id'   => 'cl_review_bonus_code',
                'type' => 'text',
            ));

            $casinos_review->add_field(array(
                'name' => 'Time to withdraw',
                'id'   => 'cl_review_withdraw',
                'type' => 'text_small',
            ));

            // Ratings

            $casinos_review->add_field(array(
                'name' => 'Bonus offer rating',
                'id'   => 'cl_review_rating_bonus',
                'type' => 'text_small',
            ));

            $casinos_review->add_field(array(
                'name' => 'Mobile experience rating',
                'id'   => 'cl_review_rating_mobile',
                'type' => 'text_small',
            ));

            $casinos_review->add_field(array(
                'name' => 'Payment options rating',
                'id'   => 'cl_review_rating_payments',
                'type' => 'text_small',
            ));

            $casinos_review->add_field(array(
                'name' => 'Customer support rating',
                'id'   => 'cl_review_rating_support',
                'type' => 'text_small',
            ));

            //Ratings end

            //Features start

            $casinos_review->add_field(array(
                'name' => 'Language',
                'id'   => 'cl_feature_language',
                'type' => 'text_small',
            ));

            $casinos_review->add_field(array(
                'name' => 'Language available?',
                'id'   => 'cl_feature_lang_available',
                'type' => 'checkbox',
            ));

            $casinos_review->add_field(array(
                'name' => 'Feature - Live casino',
                'id'   => 'cl_feature_live_casino',
                'type' => 'checkbox',
            ));

            $casinos_review->add_field(array(
                'name' => 'Feature - Tax',
                'id'   => 'cl_feature_tax',
                'type' => 'checkbox',
            ));

            $casinos_review->add_field(array(
                'name' => 'Feature - Betting',
                'id'   => 'cl_feature_betting',
                'type' => 'checkbox',
            ));

            $casinos_review->add_field(array(
                'name' => 'Feature - Bingo',
                'id'   => 'cl_feature_bingo',
                'type' => 'checkbox',
            ));

            $casinos_review->add_field(array(
                'name' => 'Feature - Scratch Cards',
                'id'   => 'cl_feature_scratch_cards',
                'type' => 'checkbox',
            ));

            $casinos_review->add_field(array(
                'name' => 'Feature - Poker',
                'id'   => 'cl_feature_poker',
                'type' => 'checkbox',
            ));

            //Features end

            //Software

            $casinos_review->add_field(array(
                'name'    => 'Software',
                'id'      => 'cl_software',
                'type'    => 'multicheck_inline',
                'options' => array(
                    'microgaming' => 'Microgaming',
                    'isoftbet' => 'Isoftbet',
                    'nektan' => 'Nektan',
                    'netent' => 'Net Ent',
                    'gig' => 'GiG',
                    'pragmatic' => 'Pragmatic',
                    'playngo' => 'Play n Go',
                    'yggdrasil' => 'Yggdrasil',
                    'quickspin' => 'Quickspin',
                    'redtiger' => 'Red Tiger',
                    'scientific' => 'Scientific',
                    'elk'  => 'ELK',
                    'rabcat' => 'RabCat',
                    'push' => 'Push Gaming',
                    'nyx' => 'NYX Gaming',
                    'thunderkick' => 'Thunderkick',
                    'playtech' => 'Playtech',
                    'bigtime' => 'Big Time Gaming',
                    'betdigital' => 'BetDigital Gaming',
                    'stormcraft'  => 'StormCraft Studios',
                    'nextgen' => 'NextGen Gaming',
                    'habanero' => 'Habanero Gaming',
                    'evolution' => 'Evolution Gaming',
                    'merkur' => 'Merkur Gaming',
                    'amaya' => 'Amaya',
                    'realtime' => 'Realtime Gaming',
                    'cryptologic'  => 'Cryptologic',
                    'igt'  => 'IGT',
                    'novomatic' => 'Novomatic',
                    'fugaso' => 'Fugaso Gaming',
                    'aristocrat' => 'Aristocrat',
                    'wms' => 'WMS',
                    'egt' => 'EGT',
                    'betsoft' => 'Betsoft Gaming',
                    'exclusive'  => 'Exclusive'

                ),
            ));

            $casinos_review->add_field(array(
                'name'    => 'Players accepted',
                'id'      => 'cl_players_accepted',
                'type'    => 'multicheck_inline',
                'options' => array(
                    'world' => 'World',
                    'europe' => 'Europe',
                    'usa' => 'USA',
                    'canada' => 'Canada',
                    'gb' => 'Great Britain',
                    'australia' => 'Australia'
                ),
            ));

            //Software end

            // ----------------------------------------------------------------------------------------- Schema Info

            $schema_info = new_cmb2_box(array(
                'id' => 'casino-schema-data',
                'title' => __('Casino schema', 'casino-listings'),
                'object_types' => array('casino-listing'),
                'context' => 'normal',
            ));

            $schema_info->add_field(array(
                'name' => 'Description',
                'id'   => 'cl_schema_description',
                'type' => 'text',
            ));

            $schema_info->add_field(array(
                'name' => 'Brand',
                'id'   => 'cl_schema_brand',
                'type' => 'text',
            ));

            $schema_info->add_field(array(
                'name' => 'Rating Count',
                'id'   => 'cl_schema_rating_count',
                'type' => 'text_small',
            ));
        }
    }
endif;