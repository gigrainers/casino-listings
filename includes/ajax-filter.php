<?php

$plugin_dir = ABSPATH . 'wp-content/plugins/casino-listings/languages/' . 'casino-listings-' . get_locale() . '.mo';
load_textdomain('casino-listings', $plugin_dir);

add_action('wp_ajax_nopriv_filter', 'filter_ajax');
add_action('wp_ajax_filter', 'filter_ajax');


function filter_ajax()
{
    $category = $_POST['category'];

    $loop_args = array(
        'post_type' => 'casino-listing',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'casino-category',
                'field'    => 'term_id',
                'terms'    => $category,
                'operator' => 'IN'
            )
        ),

    );

    $loop = new WP_Query($loop_args);

    while ($loop->have_posts()) :
        $loop->the_post();
        include CASINO_LISTINGS_BASE_DIR . 'templates/shortcodes/casino-list-toplist.php';
    // End the loop.
    endwhile;
    wp_reset_postdata();

    die();
}
