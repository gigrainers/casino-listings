<?php

if (!defined('ABSPATH')) {
    die;
}

if (!class_exists('Casino_Listings_Shortcodes')) {

    class Casino_Listings_Shortcodes
    {

        private $plugin_name;

        private $version;


        public function __construct($plugin_name, $version)
        {
            $this->plugin_name = $plugin_name;

            $this->version = $version;

            $this->setup_hooks();
        }

        /**
         * Setup action/filter hooks
         * 
         */
        public function setup_hooks()
        {
            add_action('wp_enqueue_scripts', array($this, 'register_style'));
        }

        /**
         * Register placeholder style
         */
        public function register_style()
        {
            wp_register_style(
                $this->plugin_name . '-shortcodes',
                CASINO_LISTINGS_PLUGIN_URL . 'public/css/casino-listings-shortcodes.css'
            );
        }


        /**
         * Shortcode for casino LIST
         */

        public function casino_list($atts)
        {
            $atts = shortcode_atts(
                array(
                    'limit' => get_option('posts_per_page'),
                    'id' => '',
                    'time' => '',
                    'single' => '',
                    'filter' => '',
                    'itemlist' => ''
                ),
                $atts,
                'casino_list'
            );

            $time_update = $atts['time'];

            $ajax_filter = $atts['filter'];

            $ids = $atts['id'];

            $ids = explode(',', $ids);

            if ($atts['single'] == "on") {
                $single_cas = "cl_single";
            } else {
                $single_cas = "";
            }

            // $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

            $loop_args = array(
                'orderby' => 'post__in date',
                'order' => 'ASC',
                'post_type' => 'casino-listing',
                'posts_per_page' => $atts['limit'],
                // 'paged' => $paged
            );

            if (!empty($atts['id'])) {
                $loop_args['post__in'] = $ids;
            }

            $loop = new WP_Query($loop_args);

            ob_start();
?>
            <div class="listings-wrapper">
                <?php include_once 'parts/filter-time.php'; ?>
                <div id='loader' style="display: none;">
                    <img src=<?php echo CASINO_LISTINGS_PLUGIN_URL . 'public/img/reload.gif'; ?>>
                </div>
                <div class="js-filter casino-container <?php echo $single_cas; ?>">
                    <div class="list-items" <?php if ($atts['itemlist'] == 'on') : echo 'itemscope itemtype="https://schema.org/ItemList"';
                                            endif; ?>>
                        <?php
                        $number = 0;
                        while ($loop->have_posts()) :
                            $number++;
                            $loop->the_post();
                            include CASINO_LISTINGS_BASE_DIR . 'templates/shortcodes/casino-list-toplist.php';
                        endwhile;
                        ?>
                    </div>
                </div>
            </div>
            <?php 
            $posts_per_page = intval($loop->query["posts_per_page"]);
            
            if ($posts_per_page == 40) echo "<div class='load-more-wrap'><span class='load-more-casinos'>Lataa lisää</span></div>"; ?>
        <?php
            // Restore original post
            wp_reset_postdata();

            return ob_get_clean();
        }


        public function casino_box($atts, $content)
        {
            $atts = shortcode_atts(
                array(
                    'limit' => get_option('posts_per_page'),
                    'id' => '',
                    'style' => ''
                ),
                $atts,
                'casino_box'
            );

            $ids = $atts['id'];

            $ids = explode(',', $ids);

            $loop_args = array(
                'orderby' => 'date',
                'order' => 'DESC',
                'post_type' => 'casino-listing',
                'posts_per_page' => $atts['limit'],

            );

            if (!empty($atts['id'])) {
                $loop_args['post__in'] = $ids;
            }

            $loop = new WP_Query($loop_args);


            $wide_css = $atts['style'];

            ob_start();
        ?>

            <div class="<?php echo $wide_css; ?>box-container">
                <?php

                while ($loop->have_posts()) :

                    $loop->the_post();
                    include CASINO_LISTINGS_BASE_DIR . 'templates/shortcodes/casino-list-box.php';
                // End the loop.
                endwhile;
                ?>
            </div>
<?php
            // Restore original post
            wp_reset_postdata();

            return ob_get_clean();
        }
    }
}
