<?php

if (!defined('ABSPATH')) {
    die;
}

if (!function_exists('cl_is_single_or_archive_casino')) :
    function cl_is_single_or_archive_casino()
    {
        return (is_singular('casino-listing') || is_post_type_archive('casino-listing')) ? true : false;
    }
endif;


if (!function_exists('cl_get_template_loader')) :
    function cl_get_template_loader()
    {
        return Casino_Listings_Global::template_loader();
    }
endif;

if (!function_exists('cl_is_archive_casino')) :
    function cl_is_archive_casino()
    {
        return (is_post_type_archive('casino-listing')) ? true : false;
    }
endif;