<?php

if (!defined('ABSPATH')) {
	die;
}

/**
 * Fired during plugin activation
 *
 * @link       https://yoursite.lv
 * @since      1.0.0
 *
 * @package    Casino_Listings
 * @subpackage Casino_Listings/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Casino_Listings
 * @subpackage Casino_Listings/includes
 * @author     Kristaps Ritins <kristaps@yoursite.lv>
 */

if (!class_exists('Casino_Listings_Activator')) :
	class Casino_Listings_Activator
	{

		/**
		 * Short Description. (use period)
		 *
		 * Long Description.
		 *
		 * @since    1.0.0
		 */
		public static function activate()
		{
			require_once CASINO_LISTINGS_BASE_DIR . 'includes/class-casino-listings-post-types.php';

			//Register CPT 
			$plugin_post_type = new Casino_Listings_Post_Types(CASINO_LISTINGS_NAME, CASINO_LISTINGS_VERSION);

			$plugin_post_type->init();
			
			//Flush permalinks
			flush_rewrite_rules();
		}
	}
endif;
