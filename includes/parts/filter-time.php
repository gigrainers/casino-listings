<div class="info-wrap">
    <?php if ($time_update == "on") { ?>
        <div class="time-updater">
            <div class="time-updated">
                <?php echo __("List updated: ", "casino-listings") . "<span>" . date_i18n("F, Y") . "</span>"; ?>
            </div>
        </div>
    <?php } ?>
    <?php
    if ($ajax_filter == "on") {
        $taxonomies = get_terms(array(
            'taxonomy' => 'casino-category',
            'hide_empty' => false
        ));

        if (!empty($taxonomies)) :
            $output = "<div class='casino-filter'><div class='filter-item-wrapper'>";

            foreach ($taxonomies as $category) {
                $term_id = $category->term_id;
                $output .= "<a data-category='{$term_id}' class='js-filter-item button-filter' href='#{$category->slug}'>" . esc_attr($category->name) . '</a>';
            }
            $output .= "</div></div>";
            echo $output;
        endif;
    }
    ?>
</div>